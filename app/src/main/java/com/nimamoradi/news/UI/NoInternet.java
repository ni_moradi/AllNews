package com.nimamoradi.news.UI;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nimamoradi.news.R;

/**
 * Created by nima on 1/23/2017.
 */

public class NoInternet extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.internet, container, false);

        getDialog().setTitle("no internet available");

        return rootView;
    }
}
