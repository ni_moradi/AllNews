package com.nimamoradi.news.Fragments;

import android.content.Context;
import android.os.Bundle;

import com.nimamoradi.news.Utils.DownloadTask;
import com.nimamoradi.news.addapter.ISNA_Addapter;
import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;
import com.nimamoradi.news.news.parser.isna.NewsParserISNA;

import java.util.List;

/**
 * Created by nima1 on 2/10/2017.
 */

public class ISNA extends BaseListFragment {


    public ISNA(Context context) {
        super(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new DownloadTask(new NewsParserISNA(context), getActivity()) {
            @Override
            public void result(List<News> result, final NewsParser parser) {
                ISNA.this.parser = parser;

                setListAdapter(new ISNA_Addapter(context, result));

            }
        }.execute();
    }
}
