package com.nimamoradi.news.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nimamoradi.news.Activites.NewsPreview;
import com.nimamoradi.news.R;
import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;


/**
 * Created by nima on 2/10/2017.
 */

public class BaseListFragment extends ListFragment {
    Context context;
    NewsParser parser;

    public BaseListFragment() {
    }


    protected BaseListFragment(Context context) {
        this.context = context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.list, container, false);

        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        News news = (News) l.getAdapter().getItem(position);

        Intent newsPreview = new Intent(context, NewsPreview.class);
        parser.getParser().link = news.getLink();
        newsPreview.putExtra(getString(R.string.NewsPageParser_in), parser.getParser());
        startActivity(newsPreview);
    }
}
