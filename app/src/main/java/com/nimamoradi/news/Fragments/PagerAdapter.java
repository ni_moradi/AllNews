package com.nimamoradi.news.Fragments;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.ListFragment;

import com.nimamoradi.news.news.parser.NewsParser;

/**
 * Created by nima1 on 2/10/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Context context;
    NewsParser parser;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;

        this.mNumOfTabs = 3;
    }

    @Override
    public ListFragment getItem(int position) {

        switch (position) {
            case 0:
                IRNA tab1 = new IRNA(context);
                return tab1;
            case 1:
                ISNA tab2 = new ISNA(context);
                return tab2;
            case 2:
                IRIB tab3 = new IRIB(context);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
