package com.nimamoradi.news.Fragments;

import android.content.Context;
import android.os.Bundle;

import com.nimamoradi.news.Utils.DownloadTask;
import com.nimamoradi.news.addapter.IRIB_Addapter;
import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;
import com.nimamoradi.news.news.parser.irib.NewsParserIRIB;

import java.util.List;

/**
 * Created by nima1 on 2/10/2017.
 */

public class IRIB extends BaseListFragment {


    public IRIB(Context context) {
        super(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new DownloadTask(new NewsParserIRIB(context), getActivity()) {
            @Override
            public void result(List<News> result, final NewsParser parser) {
                IRIB.this.parser = parser;

                setListAdapter(new IRIB_Addapter(context, result));


                //   getListView().setOnItemClickListener(super.);
            }
        }.execute();
    }
}
