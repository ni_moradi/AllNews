package com.nimamoradi.news.Fragments;

import android.content.Context;
import android.os.Bundle;

import com.nimamoradi.news.Utils.DownloadTask;
import com.nimamoradi.news.addapter.NewsAddapter;
import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;
import com.nimamoradi.news.news.parser.irna.NewsParserIRNA;

import java.util.List;

/**
 * Created by nima1 on 2/9/2017.
 */

public class IRNA extends BaseListFragment {

    public IRNA(Context context) {
        super(context);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new DownloadTask(new NewsParserIRNA(context), getActivity()) {
            @Override
            public void result(List<News> result, final NewsParser parser) {
                IRNA.this.parser = parser;

                setListAdapter(new NewsAddapter(context, result));

            }
        }.execute();
    }

}
