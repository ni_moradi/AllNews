package com.nimamoradi.news.Utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by nima1 on 2/9/2017.
 */

public abstract class DownloadTask extends
        AsyncTask<Void, Void, List<News>> {


    Activity activity;
    private NewsParser parser;
    public DownloadTask(NewsParser parser, Activity activity) {
        this.activity=activity;
        this.parser = parser;
    }

    @Override
    protected List<News> doInBackground(Void... urls) {
        try {
            return download();
        } catch (IOException e) {
            //      System.exit(1);
            return null;
        }
    }


    @Override
    protected void onPostExecute(List<News> result) {
        try {
           result.size();
            result(result, parser);
        }catch (java.lang.NullPointerException e){
          InternetTools.NoInternet(activity);

        }
        finally {

        }
    }
public abstract void  result(List<News> result,NewsParser parser);

    @Nullable
    private List<News> download() throws IOException {
        InputStream inputStream = null;
        HttpURLConnection conn = null;
        try {

          String  link = parser.getLinkNewsStation();
            URL url = new URL(link);
            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                inputStream = conn.getInputStream();


                return parser.getNews(inputStream);
            }

        } catch (Exception e) {
            //  System.exit(1);
            return null;
        } finally {
            conn.disconnect();
            if (inputStream != null) {
                inputStream.close();
            }
        }
        //  System.exit(1);
        return null;
    }


}