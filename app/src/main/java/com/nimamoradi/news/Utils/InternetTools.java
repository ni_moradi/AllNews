package com.nimamoradi.news.Utils;

import android.app.Activity;
import android.app.FragmentManager;

import com.nimamoradi.news.UI.NoInternet;

import java.net.InetAddress;

/**
 * Created by nima1 on 3/2/2017.
 */

public class InternetTools {
    public static boolean isAnyNetworkConnected() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }

    public static void NoInternet(Activity context) {
        FragmentManager fm = context.getFragmentManager();
        NoInternet dialogFragment = new NoInternet();
        dialogFragment.show(fm, "no internet");
    }
}
