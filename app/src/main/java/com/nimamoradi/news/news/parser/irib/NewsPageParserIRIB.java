package com.nimamoradi.news.news.parser.irib;

import com.nimamoradi.news.Constants.Html_css;
import com.nimamoradi.news.news.parser.NewsPageParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by nima1 on 1/30/2017.
 */

public class NewsPageParserIRIB extends NewsPageParser {
    @Override
    public String getHtml(String param) {
//        URL url;
//        InputStream is = null;
//        BufferedReader br;
//        String line = "";
//        StringBuilder text = new StringBuilder();
//
//        try {
//            url = new URL(param);
//            is = url.openStream();  // throws an IOException
//            br = new BufferedReader(new InputStreamReader(is));
//
//            while ((line = br.readLine()) != null) {
//                text.append(line);
//            }
        Document doc = null;


        try {
            doc = Jsoup.connect(param).get();


            //  doc.select("a").unwrap();

            Elements ps = doc.select("div.body");

            StringBuilder builder = new StringBuilder();
            builder.append(Html_css.Style);
            builder.append("<body>");
            builder.append(Jsoup.clean(ps.outerHtml(), "http://www.iribnews.ir/", Whitelist.basicWithImages()));
            builder.append(" </body>\n" +
                    "</html>");

            return builder + "";


        } catch (IOException ioe) {
            ioe.printStackTrace();

        }
       return null;
    }




}
