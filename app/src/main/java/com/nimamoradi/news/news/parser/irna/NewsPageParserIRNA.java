package com.nimamoradi.news.news.parser.irna;

import com.nimamoradi.news.Constants.Html_css;
import com.nimamoradi.news.news.parser.NewsPageParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by nima1 on 1/30/2017.
 */

public class NewsPageParserIRNA extends NewsPageParser {


    InputStream is = null;

    private String Connect(String url) throws IOException {



        try {

            Document doc = null;


            //    doc = Jsoup.parse(Download(url).toString());
            doc = Jsoup.connect(url).get();
            //  doc.select("a").remove();

            doc.select("div.Tags").remove();
            Elements ps = doc.select("div.BodyText");
            StringBuilder builder = new StringBuilder();
            builder.append(Html_css.Style);
            builder.append("<body>");
            builder.append(Title(doc));
            builder.append(ps.outerHtml());
            builder.append(" </body>\n" +
                    "</html>");

            return builder + "";


        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return null;
    }

    private StringBuilder Download(String address) throws IOException {
        URL url;
        String line = "";
        StringBuilder text = new StringBuilder();


        BufferedReader br;
        url = new URL(address);
        is = url.openStream();  // throws an IOException
        br = new BufferedReader(new InputStreamReader(is));

        while ((line = br.readLine()) != null) {
            text.append(line);
        }

        return  text;
    }
    private String Title(Document doc){

        String result="";
        result +=doc.select("div.ContentStyle>h3").outerHtml();

        result+=doc.select("div.ContentStyle>h1").outerHtml();
        result=doc.select("div.ContentStyle>h4").outerHtml();

        return result;



    }

    @Override
    public String getHtml(String param) {


        try {
            return Connect(param);
        } catch (IOException e) {
            e.printStackTrace();
            //// TODO: 2/2/2017
        }
    return null;
    }
}
