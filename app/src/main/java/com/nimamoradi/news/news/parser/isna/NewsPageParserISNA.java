package com.nimamoradi.news.news.parser.isna;

import com.nimamoradi.news.Constants.Html_css;
import com.nimamoradi.news.news.parser.NewsPageParser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by nima1 on 1/30/2017.
 */

public class NewsPageParserISNA extends NewsPageParser {
    @Override
    public String getHtml(String param) {
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line = "";
        StringBuilder text = new StringBuilder();

        try {

            Document doc = null;

            doc = Jsoup.connect(param).get();
            Elements ps = doc.select("div.item-body");
            StringBuilder builder = new StringBuilder();
            builder.append(Html_css.Style);
            builder.append("<body>");
            builder.append(ps.outerHtml());
            builder.append(" </body>\n" +
                    "</html>");

            return builder + "";


        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return null;
    }
}
