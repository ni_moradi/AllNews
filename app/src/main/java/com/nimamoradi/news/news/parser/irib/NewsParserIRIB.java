package com.nimamoradi.news.news.parser.irib;

import android.content.Context;

import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsPageParser;
import com.nimamoradi.news.news.parser.NewsParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

;

/**
 * Created by Mahdi on 1/19/2017.
 */
public class NewsParserIRIB extends NewsParser {

    public static final String IRIB_LINK = "http://www.iribnews.ir/fa/rss/allnews";
    News newsItem = new News();
    private NewsPageParserIRIB secondParser;

    public NewsParserIRIB(Context context) {
        super(context);

    }

    @Override
    protected String NewsStationName() {
        return "IRIB";
    }

    @Override
    protected List<News> parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<News> news = new ArrayList<>();
        int event = parser.next();


        skipToItems(parser);
        while (event != XmlPullParser.END_DOCUMENT) {
            if (event == XmlPullParser.START_TAG) {

                if ("title".equalsIgnoreCase(parser.getName())) {
                    newsItem.setTitle(parser.nextText());
                } else if ("link".equalsIgnoreCase(parser.getName())) {
                    newsItem.setLink(parser.nextText());

                } else if ("description".equalsIgnoreCase(parser.getName())) {
                    newsItem.setText(parser.nextText());
                } else if ("enclosure".equalsIgnoreCase(parser.getName())) {

                    newsItem.setPic(parser.getAttributeValue(0));

                } else if ("pubDate".equalsIgnoreCase(parser.getName())) {
                    newsItem.setDate(newsItem.stringTOdate(parser.nextText()));
                    newsItem.setNewsStation(NewsStationName());
                }
            }
            if (newsItem.getText() != null && newsItem.getLink() != null && newsItem.getTitle() != null && newsItem.getPic() != null && newsItem.getDate() != null) {
                news.add(newsItem);
                newsItem = null;
                newsItem = new News();
                //   continue;
            }

          else  if (newsItem.getText() != null&& newsItem.getPic() == null && newsItem.getLink() != null && newsItem.getTitle() != null && newsItem.getDate() != null) {
                news.add(newsItem);
                newsItem = null;
                newsItem = new News();
             //   continue;
            }
           else if (newsItem.getText() == null&& newsItem.getLink() != null&&newsItem.getPic() != null && newsItem.getLink() != null && newsItem.getTitle() != null && newsItem.getDate() != null) {
                news.add(newsItem);
                newsItem = null;
                newsItem = new News();
                // continue;
            }
            else if ( newsItem.getText() == null&&newsItem.getLink() != null && newsItem.getTitle() != null&& newsItem.getPic() == null  && newsItem.getDate() != null) {
                news.add(newsItem);
                newsItem = null;
                newsItem = new News();
                // continue;
            }



                event = parser.next();
        }
        return news;
    }

    @Override
    public NewsPageParser getParser() {
        return secondParser = (secondParser == null) ?
                new NewsPageParserIRIB() :
                secondParser;
    }


    @Override
    public String getLinkNewsStation() {
        return IRIB_LINK;
    }


}
