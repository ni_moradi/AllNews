package com.nimamoradi.news.news.parser;

import java.io.Serializable;

/**
 * Created by nima1 on 1/30/2017.
 */

public abstract class NewsPageParser implements Serializable {
    public String link;

    public abstract String getHtml(String param);

}
