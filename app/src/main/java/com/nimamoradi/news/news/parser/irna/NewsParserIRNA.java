package com.nimamoradi.news.news.parser.irna;

import android.content.Context;

import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsPageParser;
import com.nimamoradi.news.news.parser.NewsParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by nima
 */
public class NewsParserIRNA extends NewsParser {

    public static final String IRNA_LINK="http://www.irna.ir/fa/rss.aspx?kind=-1";
    protected String title = null, text = null;
    protected String link = null;
    protected String pic = null;
    protected String date = null;
    private NewsPageParserIRNA secondParser;

    public NewsParserIRNA(Context context){
        super(context);

    }

    @Override
    protected String NewsStationName() {
        return "IRNA";
    }

    @Override
    public NewsPageParser getParser() {
        return secondParser = (secondParser == null) ?
                new NewsPageParserIRNA() :
                secondParser;
    }

    @Override
    protected List<News> parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<News> news = new ArrayList<>();
        int event = parser.next();
        boolean description=false;


        skipToItems(parser);
        while (event != XmlPullParser.END_DOCUMENT) {
            if (event == XmlPullParser.START_TAG) {
                if ("title".equalsIgnoreCase(parser.getName())) {
                    title = parser.nextText();
                } else if ("link".equalsIgnoreCase(parser.getName())) {
                    link = parser.nextText();
                }else if ("description".equalsIgnoreCase(parser.getName())) {
                    text = parser.nextText();
                    description=true;
                    String[] s=text.split("</a>");
                    text = s[s.length - 1].replace("</p><br clear='all'/>", "");
                }else if ("a".equalsIgnoreCase(parser.getName())&&description) {
                    text = parser.nextText();
                } else if ("media:content".equalsIgnoreCase(parser.getName())) {
                    pic = parser.getAttributeValue(0);
                }else if ("pubDate".equalsIgnoreCase(parser.getName())) {
                    date = parser.nextText();
                }
            }
            if (title != null && link != null && text != null && date != null && (pic != null || "item".equalsIgnoreCase(parser.getName()))) {
                news.add(new News(title, text, date, link, pic));
                title = null ; link = null;text =null;pic=null;date=null;description=false;
            }
            event = parser.next();
        }
        return news;
    }

    @Override
    public String getLinkNewsStation() {
        return IRNA_LINK;
    }

}

