package com.nimamoradi.news.news.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class News {
    private String title, text;
    private String link;
    private String pic;
    private String newsStation;
    private Date date;
    private long id;

    public News(String title, String text, String date, String link, String pic) {
        this.title = title;
        this.text = text;
        this.date = stringTOdate(date);
        this.link = link;
        this.pic = pic;
    }

    public News() {
    }

    public String getNewsStation() {
        return newsStation;
    }

    public void setNewsStation(String newsStation) {
        this.newsStation = newsStation;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }



    public void setText(String text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public Date stringTOdate(String s) {
        SimpleDateFormat formatter1 = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd MMM yyyy hh:mm:ss");
        Date date = null;
        try {
            date = formatter1.parse(s);
        } catch (ParseException e) {
            try {
                date = formatter2.parse(s);
            } catch (ParseException l) {

            }
        }
        return date;
    }


}


