package com.nimamoradi.news.news.parser;

import android.content.Context;
import android.util.Xml;

import com.nimamoradi.news.news.model.News;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Mahdi on 1/19/2017.
 */
public abstract class NewsParser{


    static int i = 0;
    private String linkNewsStation;
    private Context context;

    public NewsParser(Context context){

     this.context=context;

    }
    protected abstract String NewsStationName();


    public List<News> getNews(InputStream xmlInputStream) {
        List<News> news = null;
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(xmlInputStream, null);
            parser.nextTag();
            news = parse(parser);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                xmlInputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return news;
    }


    protected void skipToItems(XmlPullParser parser) {
        try {
            int event = parser.next();
            while (event != XmlPullParser.END_DOCUMENT) {
                if (event == XmlPullParser.START_TAG) {
                    if ("item".equalsIgnoreCase(parser.getName())) {
                        return;
                    }
                }
                event = parser.next();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    public Context getContext(){
        return context;
    }
    public abstract String getLinkNewsStation();
    protected abstract  List<News> parse(XmlPullParser parser) throws XmlPullParserException, IOException;
    public abstract NewsPageParser getParser();


}
