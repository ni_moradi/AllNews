package com.nimamoradi.news.news.parser.isna;

import android.content.Context;

import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsPageParser;
import com.nimamoradi.news.news.parser.NewsParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mahdi on 1/19/2017.
 */
public class NewsParserISNA extends NewsParser {
    public static final String ISNA_LINK="http://www.isna.ir/rss?pl=1";
    private NewsPageParserISNA secondParser;
    public NewsParserISNA(Context context){
        super(context);

    }

    @Override
    protected String NewsStationName() {
        return "ISNA";
    }

    @Override
    protected List<News> parse(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<News> news = new ArrayList<>();
        int event = parser.next();
News newsItem=new News();


        skipToItems(parser);
        while (event != XmlPullParser.END_DOCUMENT) {
            if (event == XmlPullParser.START_TAG) {
                if ("title".equalsIgnoreCase(parser.getName())) {
                    newsItem.setTitle(parser.nextText());
                } else if ("link".equalsIgnoreCase(parser.getName())) {
                    newsItem.setLink(parser.nextText());
                }else if ("description".equalsIgnoreCase(parser.getName())) {
                    newsItem.setText(parser.nextText());
                }else if ("enclosure".equalsIgnoreCase(parser.getName())) {
                  //  pic=saveToInternalStorage(parser.getAttributeValue(0),"ISNA");

                    newsItem.setPic(parser.getAttributeValue(0));
                }else if ("pubDate".equalsIgnoreCase(parser.getName())) {
                    newsItem.setNewsStation(NewsStationName());
                  newsItem.setDate(newsItem.stringTOdate(parser.nextText()));
                }
            }
            if(newsItem.getText() != null &&
                    newsItem.getLink() != null&&newsItem.getTitle()!=null
                        &&newsItem.getPic()!=null&&newsItem.getDate()!=null) {
                news.add(newsItem);
                newsItem=null;
newsItem=new News();
            }
            event = parser.next();
        }
        return news;
    }

    @Override
    public NewsPageParser getParser() {
        return secondParser = (secondParser == null) ?
                new NewsPageParserISNA() :
                secondParser;
    }


    @Override
    public String getLinkNewsStation() {
        return ISNA_LINK;
    }


}
