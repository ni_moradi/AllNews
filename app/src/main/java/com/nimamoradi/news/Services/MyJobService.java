package com.nimamoradi.news.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.nimamoradi.news.R;

/**
 * Created by Mahdi on 1/20/2017.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {
    private int notificationNumber = 0;

    private Thread thread;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onStartJob(JobParameters params) {
        thread = new WorkerThread(this, params);
        thread.start();
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        createNotification("MyJobService onStop with JobId");
        return false;
    }

    public void createNotification(String msg) {
        notificationNumber++;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentText(msg);
        builder.setContentTitle("New Notification #" + notificationNumber);
        builder.setSmallIcon(R.drawable.ic_menu_share);
        builder.setAutoCancel(true);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = builder.build();
        manager.notify(notificationNumber, notification);
    }

}

class WorkerThread extends Thread {
    private MyJobService service;
    private JobParameters params;
    public WorkerThread(MyJobService service, JobParameters params) {
        this.service = service;
        this.params = params;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void run() {
        try {
            service.createNotification("MyJobService.WorkerThread Started");
            Thread.sleep(10000);

            service.createNotification("MyJobService.WorkerThread Finished");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.i("##########", "Job Finished");
            service.jobFinished(params, false);
        }
    }
}

