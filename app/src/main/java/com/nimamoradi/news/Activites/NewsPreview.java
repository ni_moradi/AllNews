package com.nimamoradi.news.Activites;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.nimamoradi.news.R;
import com.nimamoradi.news.news.parser.NewsPageParser;


public class NewsPreview extends AppCompatActivity {
    NewsPageParser newsPageParser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_preview);
       Intent newsPreview= getIntent();
        newsPageParser = (NewsPageParser) newsPreview.getSerializableExtra(getString(R.string.NewsPageParser_in));
        backgroundTask task=new backgroundTask();
        task.execute(newsPageParser.link);

    }
    class backgroundTask extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            return newsPageParser.getHtml(params[0]);

        }






        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            WebView webView= (WebView) findViewById(R.id.webview);
            webView.loadData(s, "text/html; charset=utf-8", "UTF-8");
        }
    }

}
