package com.nimamoradi.news.Activites;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.nimamoradi.news.Fragments.PagerAdapter;
import com.nimamoradi.news.R;
import com.nimamoradi.news.Utils.DownloadTask;
import com.nimamoradi.news.Utils.InternetTools;
import com.nimamoradi.news.news.model.News;
import com.nimamoradi.news.news.parser.NewsParser;
import com.nimamoradi.news.news.parser.irib.NewsParserIRIB;

import java.util.List;


public class Main extends NavActivity {
    private static final String NET_TEST_TAG = "NetTest";

    String link;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), this);

        viewPager.setAdapter(adapter);
        //Adding onTabSelectedListener to swipe views

        tabLayout.setupWithViewPager(viewPager, false);
        tabLayout.getTabAt(0).setText(R.string.IRNA);
        tabLayout.getTabAt(1).setText(R.string.ISNA);
        tabLayout.getTabAt(2).setText(R.string.IRIB);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);






        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int result = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
            if (result == PackageManager.PERMISSION_GRANTED) {
                Log.e("permission", "have permission .INTERNET");
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                        100
                );
                Log.e("permission", "don't have .INTERNET  permission");
                ActivityCompat.requestPermissions(Main.this,
                        new String[]{Manifest.permission.INTERNET},
                        1);
            }
        }
        //   load();
    }


    private void load() {
        if (InternetTools.isAnyNetworkConnected()) {

            new DownloadTask(new NewsParserIRIB(this), this) {
                @Override
                public void result(List<News> result, final NewsParser parser) {
                }
            }
                    .execute();
        } else {
            InternetTools.NoInternet(this);

        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (InternetTools.isAnyNetworkConnected())
            load();
        }
    }


}