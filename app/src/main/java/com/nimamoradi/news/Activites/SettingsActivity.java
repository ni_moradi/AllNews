package com.nimamoradi.news.Activites;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.nimamoradi.news.R;
import com.nimamoradi.news.Services.MyJobService;



public class SettingsActivity extends Activity {


    private int JOB_ID = 1576;
    private int hour = 7;
    private int lenght = 1000 * 60 * 60;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settinglayout);
        SharedPreferences preferences=getSharedPreferences(String.valueOf(R.string.settings), Context.MODE_PRIVATE);
      EditText editday= (EditText) findViewById(R.id.settings_day);
       EditText editlenght= (EditText) findViewById(R.id.settings_lenght);
        EditText editfilter= (EditText) findViewById(R.id.settings_filter);

        editday.setText(preferences.getString(getResources().getString(R.string.settings_days),"1"));
        editlenght.setText(preferences.getString(getResources().getString(R.string.settings_lenght),"7"));
        editfilter.setText(preferences.getString(getResources().getString(R.string.settings_filter),""));

        Button button= (Button) findViewById(R.id.button_service);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartService(savedInstanceState);
            }
        });


    }

    @Override
    public void onBackPressed() {
        SharedPreferences preferences=getSharedPreferences(String.valueOf(R.string.settings), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        EditText editday= (EditText) findViewById(R.id.settings_day);
        EditText editlenght= (EditText) findViewById(R.id.settings_lenght);
        EditText editfilter= (EditText) findViewById(R.id.settings_filter);

        editor.putString(String.valueOf(R.string.settings_filter),editfilter.getText().toString());
        editor.putString(String.valueOf(R.string.settings_days),editday.getText().toString());
        editor.putString(String.valueOf(R.string.settings_lenght),editlenght.getText().toString());



        super.onBackPressed();
    }

    protected void StartService(Bundle savedInstanceState) {
        SharedPreferences preferences=getSharedPreferences(String.valueOf(R.string.settings), Context.MODE_PRIVATE);

        hour=Integer.parseInt(preferences.getString(getResources().getString(R.string.settings_lenght),"1"));
        lenght *= hour;
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_test);



        ComponentName jobService =
                new ComponentName(getPackageName(), MyJobService.class.getName());



//        PersistableBundle bundle = new PersistableBundle();
//        //sending data to job service
//        FileOutputStream output = null;
//        try {
//            output = openFileOutput("data", Context.MODE_PRIVATE );
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        DataOutputStream output2 = new DataOutputStream(output);
//
//        try {
//            byte[] array= NewsDAO.serialize(this);
//            bundle.putInt("size",array.length);
//
//            output2.write(NewsDAO.serialize(this));
//            output2.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        JobInfo job = new JobInfo.Builder(JOB_ID, new ComponentName(this, MyJobService.class))
//                // .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
//                .setPersisted(true)
//                .setRequiresCharging(true)
//                .setPeriodic(6000)
//                .setExtras(bundle)  //  bundle somehow contain myObject
//                .build();
//        JobScheduler jobScheduler;
//        jobScheduler = (JobScheduler)getSystemService(JOB_SCHEDULER_SERVICE);
//        int jobId = jobScheduler.schedule(job);
//        if(jobScheduler.schedule(job)>0){
//            Toast.makeText(this,
//                    "Successfully scheduled job: " + jobId,
//                    Toast.LENGTH_SHORT).show();
//        }else{
//            Toast.makeText(this,
//                    "RESULT_FAILURE: " + jobId,
//                    Toast.LENGTH_SHORT).show();
//        }

onBackPressed();
    }



}
