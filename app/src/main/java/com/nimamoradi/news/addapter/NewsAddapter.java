package com.nimamoradi.news.addapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nimamoradi.news.R;
import com.nimamoradi.news.news.model.News;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;


/**
 * Created by nima on 1/19/2017.
 */

public class NewsAddapter extends ArrayAdapter<News> {
    private int layout = R.layout.listitem;
    public NewsAddapter(Context context, List<News> objects) {
        super(context, R.layout.listitem, objects);
    }

    public NewsAddapter(Context context, List<News> objects, int layout) {
        super(context, R.layout.listitem, objects);
        this.layout = layout;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(layout, parent, false);
            ViewHolder viewHolder = new ViewHolder(
                    (TextView) convertView.findViewById(R.id.titleTextView),
                    (TextView) convertView.findViewById(R.id.textTextView),
                    (TextView) convertView.findViewById(R.id.dateTextView),
                    (TextView) convertView.findViewById(R.id.newsStationTextView),
                    (ImageView) convertView.findViewById(R.id.picImageView)
            );
            convertView.setTag(viewHolder);
        }
        News item = getItem(position);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag();
        viewHolder.titleTextView.setText(item.getTitle());
        viewHolder.textTextView.setText(item.getText());
        viewHolder.dateTextView.setText(item.getDate().toString());
        viewHolder.newsStationTextView.setText(item.getNewsStation() + "");
        if (item.getPic() != null) {
            Picasso.with(getContext()).load(item.getPic()).into(viewHolder.picImageView);
            viewHolder.picImageView.setVisibility(View.VISIBLE);
        }
        else viewHolder.picImageView.setVisibility(View.GONE);
        return convertView;
    }

//    @NonNull
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        if (convertView == null) {
//
//            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            convertView = layoutInflater.inflate(R.layout.listitem, parent, false);
//            ViewHolder viewHolder = new ViewHolder(
//                    (TextView) convertView.findViewById(R.id.titleTextView),
//                    (TextView) convertView.findViewById(R.id.textTextView),
//                    (TextView) convertView.findViewById(R.id.dateTextView),
//                    (TextView) convertView.findViewById(R.id.newsStationTextView),
//                    (ImageView) convertView.findViewById(R.id.picImageView)
//            );
//            convertView.setTag(R.string.TagId, viewHolder);
//
//        }
//
//        return initView(convertView,position);
//    }

    private View initView(View convertView, int position) {

        News item = getItem(position);
        ViewHolder viewHolder = (ViewHolder) convertView.getTag(R.string.TagId);
        viewHolder.titleTextView.setText(item.getTitle());
        viewHolder.textTextView.setText(item.getText());
        viewHolder.dateTextView.setText(item.getDate().toString());
//        viewHolder.favoriteTextView.setText(item.isFavorite()+"");
        viewHolder.newsStationTextView.setText(item.getNewsStation() + "");
        if (item.getPic() != null)
            Picasso.with(getContext()).load(item.getPic()).into(viewHolder.picImageView);
        else viewHolder.picImageView.setVisibility(View.GONE);
        return convertView;
    }

    public Bitmap loadImageFromStorage(String name) {
        Bitmap b;

        try {
            File f = new File(name);
            b = BitmapFactory.decodeStream(new FileInputStream(f));
            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeResource(getContext().getResources(), R.drawable.news);
    }

    private class ViewHolder {
        TextView newsStationTextView, titleTextView, textTextView, dateTextView;
        ImageView picImageView;

        public ViewHolder(TextView titleTextView, TextView textTextView, TextView dateTextView, TextView newsStationTextView, ImageView picImageView) {
            this.titleTextView = titleTextView;
            this.textTextView = textTextView;
            this.dateTextView = dateTextView;
            this.newsStationTextView = newsStationTextView;

            this.picImageView = picImageView;
        }
    }
}
