package com.nimamoradi.news.addapter;

import android.content.Context;

import com.nimamoradi.news.R;
import com.nimamoradi.news.news.model.News;

import java.util.List;

/**
 * Created by nima1 on 2/23/2017.
 */

public class IRIB_Addapter extends NewsAddapter {
    public IRIB_Addapter(Context context, List<News> objects) {
        super(context, objects, R.layout.irib_list);
    }
}
